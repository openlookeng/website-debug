import { request } from "@/shared/axios";
import type { AxiosResponse } from "@/shared/axios";

/**
 * 获取授权的相关回调链接
 */

// export function getToken() {
//   const url = "/intern/admin/login";
//   return request
//     .post(url, {
//       userName: "admin",
//       passWord: "admin",
//     })
//     .then((res: AxiosResponse) => res.data);
// }
interface rankParams {
  start: string;
  end: string;
}

export function getRank(rankParams: rankParams) {
  const url = "https://omapi.osinfra.cn/query/issueScore";
  // console.log(rankParams);
  if (rankParams.start !== "2022-9-01") {
    return request
      .get(url, {
        params: {
          community: "openlookeng",
          start_date: rankParams.start,
          end_date: rankParams.end,
        },
      })
      .then((res: AxiosResponse) => res.data);
  } else {
    return request
      .get(url, {
        params: {
          community: "openlookeng",
        },
      })
      .then((res: AxiosResponse) => res.data);
  }
}
// export function postQuestion(rankParams: rankParams) {
//   const url = "/query/issueScore";
//   return request
//     .post(url, {
//       body: {
//         community: "openEuler",
//         start_date: rankParams.start,
//         end_date: rankParams.end,
//       },
//     })
//     .then((res: AxiosResponse) => res.data);
// }
