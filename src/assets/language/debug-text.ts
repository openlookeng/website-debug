export const introduction = {
  title: "简介",
  paragraph:
    "为进一步提升大家对openLooKeng文档、博客等资源的使用体验，openLooKeng社区现推出“捉虫”有奖活动，希望通过朋友们的参与，不断改进和完善文档内容，从而帮助开发者更好地使用openLooKeng。在这里，您可以提出有效可行的体验建议，您对每个错别字的修正都会得到社区的肯定，成为社区贡献者。因此，这里也是小伙伴参与开源贡献的入门小天地，欢迎每一位小伙伴的参与。",
  activeDate: "活动时长：",
  date: "2022年6月1日-8月30日",
  activeObject: "“捉虫”对象：",
  objectList: ["openLooKeng社区文档", "openLooKeng官网博客"],
};
export const howToJoin = {
  title: "如何捉虫",
  stepTitleOne: "找Bug,提Issue",
  stepFirst: [
    "1. 通过",
    "openLooKeng社区文档",
    "或",
    "官网博客",
    "阅读自己感兴趣的内容；",
  ],
  stepTwoText:
    "2. 选中存在问题的文档片段，单击出现的图标，在弹出窗口中填写相关信息 。",
  stepThreeText: "3. 单击“创Issue赢奖品”按钮，跳转到相应的仓库创建Issue；",
  stepFourText: [
    "4. 登录Gitee，选择Issue类型“有奖捉虫”，按要求输入标题与内容（标题请备注：有奖捉虫），完成Issue创建。",
    "可参考",
    "openLooKeng社区Issue提交指南。",
  ],
  stepTitleTwo: "提PR，解Bug",
  stepTextOne: "1. 确定需要解决的Issue，复制该Issue编号或链接；",
  stepTextTwo: [
    "2. Fork相关的Gitee仓库并修改问题：",
    "点此前往社区文档的代码仓",
    "点此前往官网博客的代码仓",
    "Tips：Star代码仓，可及时获取代码动态消息",
  ],
  stepTextThree: [
    "3. 创建PR，粘贴对应的Issue编号或链接（标题请备注：有奖捉虫），提交PR。",
    "可参考",
    "openLooKeng社区PR提交指南。",
  ],
};
export const rule = {
  title: "积分规则",
  secondTitle: "积分说明：",
  secondDetail:
    "本次“捉虫”活动采用积分制，对有效的 Issue 和 Pull Request (PR) 进行积分，参与者需达到10分以上才可以参与评奖。",
  explainList: [
    "经主办方评估采纳的问题或建议，并带有 “有奖捉虫”标签，即为有效的Issue；根据反馈的内容，每个Issue的积分1~10分不等。",
    "经主办方评估采纳，符合合入标准，并明确带有上述有效Issue编号或链接，即为有效的PR；根据反馈的内容，每个PR的积分1~12分不等。",
    "若多位参与者提交的Issue/PR，内容相似或一致，则最早提交者获取该积分，其他提交者+1分。",
    "活动定期更新积分排名，结束时组织方将依据贡献成果与最终排名进行评奖。",
  ],
  hint: "【重要提示】",
  hintList: [
    "产品问题、咨询类、纯吐槽类等不属于活动范畴内的，将不被采纳计分。",
    "问题描述需具体明确，方便快速定位和改进。如果能够附上您的优化方案或改进建议，还可参与特殊贡献奖的评奖。",
  ],
  formTitle: ["序号", "积分项内容", "积分内容"],
  formBody: [
    {
      itemTitle: "规范和低错类，如：",
      itemList: [
        "错别字或拼写错误；标点符号使用错误",
        "链接错误、空单元格、格式不对",
        "英文中包含中文字符",
        "界面和描述不一致，但不影响操作",
        "表述不通顺，但不影响理解",
        "版本号不匹配：如软件包名称、界面版本号",
      ],
      itemContent: ["每个 issue 2 分", "每个 PR 3 分"],
    },
    {
      itemTitle: "易用性问题，如：",
      itemList: [
        "关键步骤错误或缺失，无法指导用户完成任务",
        "缺少必要的前提条件、注意事项等",
        "描述存在歧义",
        "图形、表格、文字等晦涩难懂",
        "逻辑不清晰，该分类、分项、分步骤的没有给出",
      ],
      itemContent: ["每个 issue 5 分", "每个 PR 8 分"],
    },
    {
      itemTitle: "正确性问题，如：",
      itemList: [
        "技术原理、功能、规格等描述和软件不一致，存在错误",
        "原理图、架构图等存在错误",
        "命令、命令参数等错误",
        "命令无法完成对应功能",
        "界面错误，无法指导操作",
      ],
      itemContent: ["每个 issue 5 分", "每个 PR 8 分"],
    },
    {
      itemTitle: "风险提示类问题，如：",
      itemList: ["对重要数据或系统存在风险的操作，缺少安全提示"],
      itemContent: ["每个 issue 8 分", "每个 PR 12 分"],
    },
    {
      itemTitle: "其他,如：",
      itemList: ["经评审对内容改进有重大贡献"],
      itemContent: ["由主办方确定"],
    },
  ],
  formula: "积分公示:",
  formulaText: "所有活动参加者的积分将在页面下方进行积分公示与排名。",
};
export const prize = {
  title: "奖项设置",
  renderData: [
    {
      imgPath: "prize-1.png",
      text: [
        "活动积分累计 Top 1 的参加者获“问鼎奖”",
        "中奖者可获得：",
        "专属虚拟徽章+",
        "HUAWEI FreeLace 无线蓝牙耳机",
      ],
      imgPrize: "prize-1-1.png",
      id: "prizedseer1",
    },
    {
      imgPath: "prize-2.png",
      text: [
        "活动积分累计 Top 2-3 的参加者获“星耀奖”",
        "中奖者可获得：",
        "专属虚拟徽章+",
        "HUAWEI 三脚架自拍杆",
      ],
      imgPrize: "prize-2-1.png",
      id: "prizedseer2",
    },
    {
      imgPath: "prize-3.png",
      text: [
        "活动积分累计 Top 4-6 的参加者获“攀登奖”",
        "中奖者可获得：",
        "专属虚拟徽章+",
        "荣耀魔方蓝牙音箱",
      ],
      imgPrize: "prize-3-1.png",
      id: "prizedseer3",
    },
  ],
  specialData: [
    {
      imgPath: "prize-4.png",
      text: [
        "经评审对文档改进有重大贡献者获“特殊贡献奖”，",
        "该奖项不定期开启。中奖者可获得：",
        "专属虚拟徽章+",
        "HUAWEI FreeLace 无线蓝牙耳机",
      ],
      id: "prizedseer4",
    },
    {
      imgPath: "prize-5.png",
      text: [
        "本活动所有提交有效Issue或者PR的小伙伴，均可参与抽奖。",
        "每月抽取3名幸运盲盒奖，获得以下任意一份社区礼品：",
        "文化衫、挎包、三合一数据线、",
        "金属书签+徽章礼盒...",
      ],
      id: "prizedseer5",
    },
  ],
};
export const rank = {
  title: "积分排名",
  debugDetail: "查看文档捉虫详情",
  blogBugDetail: "查看博客捉虫详情",
  debuggerLink: "https://gitee.com/openlookeng/hetu-core/issues",
  blogBugLink: "https://gitee.com/openlookeng/website-v2/issues",
  choiceMonth: "选择月份",
  monthList: ["6月", "7月", "8月", "全部"],
  rankTitle: ["名次", "Gitee ID", "总分", "徽章"],
  extend: "查看完整榜单 ",
  putAway: "收起",
  empty: "暂无数据",
  // 幸运奖获得者的Gitee ID
  luckyList: [""],
  // 特殊贡献奖获得者的Gitee ID
  specialList: [""],
};
export const navList = [
  {
    key: "#introduction",
    name: "简介",
  },
  {
    key: "#join",
    name: "如何捉虫",
  },
  {
    key: "#rule",
    name: "积分规则",
  },
  {
    key: "#prize",
    name: "奖项设置",
  },
  {
    key: "#rank",
    name: "积分排名",
  },
];
export const footer = {
  copyright: "版权所有 © 2022 openLooKeng 保留一切权利",
  email: "contact@openlookeng.io",
  code_text: "关注我们",
  follow: "遵循",
  agreement: "协议",
  wechart: "微信公众号",
  community: "社群小助手",
  footerOptions: [
    {
      id: "brand",
      label: "品牌",
      link: "https://openlookeng.io/zh/brand/",
    },
    {
      id: "privacypolicy",
      label: "隐私政策",
      link: "https://openlookeng.io/zh/privacy/",
    },
    {
      id: "legalnotice",
      label: "法律申明",
      link: "https://openlookeng.io/zh/legal/",
    },
    {
      id: "serviceStatus",
      label: "服务状态",
      link: "https://status.openlookeng.io/",
    },
  ],
  apacheVersion: "Apache License Version 2.0",
  apacheVersionLink: "http://www.apache.org/licenses/LICENSE-2.0",
  bilibiliLive: "https://live.bilibili.com/22366478",
};
