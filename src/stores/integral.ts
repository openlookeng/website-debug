export function dateHandle(month: any) {
  // 月份查询
  const date = new Date();
  const year = date.getFullYear();
  let startData = "";
  let endData = "";
  const monthDate = new Date(year, month, 0);
  month < 9 ? (month = `0${month}`) : "";
  startData = `${year}-${month}-01`;
  endData = `${year}-${month}-${monthDate.getDate()}`;

  return { start: startData, end: endData };
}
export function globalVariable() {
  // 首页地址
  const homeUrl = "https://openlookeng.io/zh/";
  // 文档地址
  const docUrl =
    "https://docs.openlookeng.io/";
  const blogUrl = `${homeUrl}information/blog/`;
  return { homeUrl: homeUrl, docUrl: docUrl, blogUrl: blogUrl };
}
