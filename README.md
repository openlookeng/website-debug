# website-debug

### 介绍

本网站为[openLooKeng 社区官网](https://openlookeng.io/)的文档及博客的有奖捉虫活动的详情介绍

### 软件架构

Vue 3 + TypeScript + Vite + Scss + Vue Router + Pinia + Axios

### 安装教程

1. 推荐使用[pnpm](https://pnpm.io/installation)安装，当前你也可以使用 npm、yarn
2. 安装依赖 `pnpm install`
3. 运行项目 `pnpm dev`

### 风格命名

1. 文件夹名称建议使用 kebab-case, 如： hello-world
2. vue 文件建议使用 PascalCase, 如： HelloWorld.vue

### 目录说明

```
|-- src

  |-- api API接口

  |-- assets 资源目录，如图片、视频等
      |-- svg-icons  svg 雪碧图图标

  |-- components 跨页面公共组件

  |-- pages 项目页面
      |-- course 页面
      |-- course-detail 子页面

  |-- shared 公共工具及文件
      |-- axios axios封装
      |-- styles 公共样式

  |-- stores 全局状态管理

  --- main.ts 入口文件
  --- router.ts 路由文件
```

### 参与贡献

1.  点击本仓库 Gitee 主页右上角的 Fork 按钮，创建个人 Fork 仓
2.  使用 git 工具把代码拉取到本地
3.  新建 Feat_xxx 分支进行开发
4.  开发完成后，提交代码到个人 Fork 仓
5.  新建 pull request,把修改以 PR 形式提交到本仓库
