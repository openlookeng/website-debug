# vue3-ts-starter

### Description

This site is the documentation and blog of [openLooKeng community official website](https://openlookeng.io/)

### Software Architecture

Vue 3 + TypeScript + Vite + Scss + Vue Router + Pinia + Axios

### Installation

1. recommend using [pnpm](https://pnpm.io/installation)，you can also use `npm`、`yarn`
2. install: `pnpm install`
3. run project: `pnpm dev`

### The style name

1. The folder name is recommended kebab-case, as： hello-world
2. vue recommended use PascalCase,as： HelloWorld.vue

### File Directory Description

```
|-- src

  |-- api API port

  |-- assets A directory of resources, such as pictures, videos, etc
      |-- svg-icons  svg Sprite icon

  |-- components Cross-page common components

  |-- pages Project page
      |-- course Page
      |-- course-detail Subpage

  |-- shared Common tools and documents
      |-- axios Axios encapsulation
      |-- styles Common style

  |-- stores Global state Management

  --- main.ts Entrance to the file
  --- router.ts The routing file
```

### Contribution

1.  Fork the repository
2.  Use git tools to pull the code locally
3.  Create Feat_xxx branch
4.  Commit your code
5.  Create Pull Request
